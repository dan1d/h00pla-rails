Rails.application.routes.draw do
  root to: "metrics#index"
  resources :metrics, only: [:index, :show] do
    resources :metrics_values, only: [:edit, :update]
  end
end
