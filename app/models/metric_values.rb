class MetricValues
  @@client = HooplaClient.hoopla_client
  attr_reader :metric_value_response

  def self.all(metric_id)
    response = @@client.get("/metrics/#{metric_id}/values", {"Accept" => "application/vnd.hoopla.metric-value-list+json"})
    metric_values = response.map {|resp| MetricValues.new(resp) }.select(&:is_user?) # easier to query by user to the api// TODO: figure that out
    metric_values
  end

  def self.find(metric_id, id)
    MetricValues.new(@@client.get("/metrics/#{metric_id}/values/#{id}", {"Accept" => "application/vnd.hoopla.metric-value+json"}))
  end

  def self.update(metric_id, id, params)
    path = "/metrics/#{metric_id}/values/#{id}"
    headers = {"Content-Type" => "application/vnd.hoopla.metric-value+json"}
    @@client.put(path, params, headers)
  end

  def initialize(metric_value_response)
    @metric_value_response = metric_value_response
  end

  def id 
    metric_value_response["href"].split("/").last
  end

  def href
    metric_value_response["href"]
  end

  def value 
    metric_value_response["value"]
  end

  def owner
    metric_value_response["owner"]
  end

  def metric
    metric_value_response["metric"]
  end

  def owner_href
    owner["href"]
  end

  def updated_at
    metric_value_response["updated_at"]
  end

  def is_user?
    owner["kind"] == "user"
  end
end
