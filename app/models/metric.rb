class Metric
  @@client = HooplaClient.hoopla_client
  attr_reader :metric_response

  def self.all
    metric_response = @@client.get("/metrics", {"Accept" => "application/vnd.hoopla.metric-list+json"})
    metric_response.map {|response| Metric.new(response) }
  end

  def self.find(id)
    metric_response = @@client.get("/metrics/#{id}", {"Accept" => "application/vnd.hoopla.metric+json"})
    Metric.new(metric_response)
  end

  def initialize(metric_response)
    @metric_response = metric_response
  end

  def name
    @name ||= metric_response["name"]
  end

  def id
    @id ||= metric_response["href"].split("/").last
  end

  def list_metric_values
    MetricValues.all(id)
  end
end
