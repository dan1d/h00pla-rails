class User
  @@client = HooplaClient.hoopla_client
  attr_reader :response

  def self.get_all
    response = @@client.get("https://api.hoopla.net/users", {"Accept" => "application/vnd.hoopla.user-list+json"})
    response.map {|resp| User.new(resp) }
  end

  def initialize(response)
    @response = response
  end

  def id
    href.split("/").last
  end

  def href
    response["href"]
  end

  def email
    response["email"]
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def first_name
    response["first_name"]
  end
  def last_name 
    response["last_name"]
  end
end
