module ApplicationHelper

  def select_user_name(metric, users)
    users.find {|user| user.href == metric.owner_href }.full_name
  end
end
