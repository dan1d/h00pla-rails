class MetricsController < ApplicationController
  
  def index
    @metrics = Metric.all
  end

  def show
    @metric = Metric.find(params[:id])
    @users = User.get_all
    @metric_values = @metric.list_metric_values
  end
end
