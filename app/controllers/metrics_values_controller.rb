class MetricsValuesController < ApplicationController

  def edit
    @metric_value = MetricValues.find(params[:metric_id], params[:id])
  end

  def update
    begin
      MetricValues.update(params[:metric_id], params[:id], permited_params)
      redirect_to metric_path(params[:metric_id])
    rescue StandardError => e
      @metric_value = MetricValues.find(params[:metric_id], params[:id])
      @error = e.message
      render :edit
    end
  end

  def permited_params
    params.permit(:value, :owner, :href, :metric, :updated_at)
  end
end
